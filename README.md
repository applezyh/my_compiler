# my_compiler
#### 这是一个个人编写的SysY语言的编译器，SysY语言是一个简化的c语言。
    test.c为测试文件，source为编译器源码，lib为输入输出链接库文件夹。编译器输出的结果会存在asm文件夹下的test.s汇编文件中。
make指令完成编译+链接的工作，make qemu指令运行编译链接后的./out/test.out程序。
#### 环境配置
    在ubuntu操作系统中执行下列指令配置环境。
```
sudo apt update
sudo apt install build-essential
sudo apt install gcc-multilib
sudo apt install clang
sudo apt install -y qemu
sudo apt install -y qemu-system
sudo apt install -y qemu-user
sudo apt-get install gcc-arm-linux-gnueabihf
```
